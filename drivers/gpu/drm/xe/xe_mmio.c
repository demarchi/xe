/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright © 2021 Intel Corporation
 */

#include "xe_mmio.h"

#include <drm/drm_managed.h>
#include <drm/xe_drm.h>

#include "xe_device.h"
#include "xe_macros.h"

#include "../i915/i915_reg.h"

#define XEHPSDV_MTCFG_ADDR		_MMIO(0x101800)
#define TILE_COUNT			REG_GENMASK(15, 8)

static int xe_set_dma_info(struct xe_device *xe)
{
	unsigned int mask_size = xe->info.dma_mask_size;
	int err;

	/*
	 * We don't have a max segment size, so set it to the max so sg's
	 * debugging layer doesn't complain
	 */
	dma_set_max_seg_size(xe->drm.dev, UINT_MAX);

	err = dma_set_mask(xe->drm.dev, DMA_BIT_MASK(mask_size));
	if (err)
		goto mask_err;

	err = dma_set_coherent_mask(xe->drm.dev, DMA_BIT_MASK(mask_size));
	if (err)
		goto mask_err;

	return 0;

mask_err:
	drm_err(&xe->drm, "Can't set DMA mask/consistent mask (%d)\n", err);
	return err;
}

static void xe_mmio_probe_vram(struct xe_device *xe)
{
	struct xe_gt *gt = to_gt(xe);

	if (!IS_DGFX(xe)) {
		gt->mem.vram.size = gt->mem.vram.io_start = 0;
		return;
	}

	gt->mem.vram.size = xe_mmio_read64(gt, GEN12_GSMBASE.reg);
	gt->mem.vram.io_start = pci_resource_start(to_pci_dev(xe->drm.dev), 2);

	drm_dbg(&xe->drm, "VRAM: %pa\n", &gt->mem.vram.size);
}

static void xe_mmio_probe_tiles(struct xe_device *xe)
{
	u32 mtcfg;
	struct xe_gt *gt = to_gt(xe);

	if (!xe->info.tile_count)
		return;

	mtcfg = xe_mmio_read64(gt, XEHPSDV_MTCFG_ADDR.reg);
	xe->info.tile_count = REG_FIELD_GET(TILE_COUNT, mtcfg) + 1;
	drm_dbg(&xe->drm, "tile_count: %d\n", xe->info.tile_count);
}


static void mmio_fini(struct drm_device *drm, void *arg)
{
	struct xe_device *xe = arg;

	pci_iounmap(to_pci_dev(xe->drm.dev), xe->mmio.regs);
}

int xe_mmio_init(struct xe_device *xe)
{
	struct xe_gt *gt = to_gt(xe);
	const int mmio_bar = 0;
	int err;

	xe->mmio.size = IS_DGFX(xe) ? SZ_4M : SZ_2M;
	xe->mmio.regs = pci_iomap(to_pci_dev(xe->drm.dev), mmio_bar,
				  xe->mmio.size);
	if (xe->mmio.regs == NULL) {
		drm_err(&xe->drm, "failed to map registers\n");
		return -EIO;
	}

	err = drmm_add_action_or_reset(&xe->drm, mmio_fini, xe);
	if (err)
		return err;

	/* 1 GT for now, 1 to 1 mapping, may change on multi-GT devices */
	gt->mmio.size = xe->mmio.size;
	gt->mmio.regs = xe->mmio.regs;

	/*
	 * The boot firmware initializes local memory and assesses its health.
	 * If memory training fails, the punit will have been instructed to
	 * keep the GT powered down; we won't be able to communicate with it
	 * and we should not continue with driver initialization.
	 */
	if (IS_DGFX(xe) && !(xe_mmio_read32(gt, GU_CNTL.reg) & LMEM_INIT)) {
		drm_err(&xe->drm, "LMEM not initialized by firmware\n");
		return -ENODEV;
	}

	err = xe_set_dma_info(xe);
	if (err)
		return err;

	xe_mmio_probe_vram(xe);
	xe_mmio_probe_tiles(xe);

	return 0;
}

#define VALID_MMIO_FLAGS (\
	DRM_XE_MMIO_BITS_MASK |\
	DRM_XE_MMIO_READ |\
	DRM_XE_MMIO_WRITE)

int xe_mmio_ioctl(struct drm_device *dev, void *data,
		  struct drm_file *file)
{
	struct xe_device *xe = to_xe_device(dev);
	struct drm_xe_mmio *args = data;
	unsigned int bits_flag, bytes;

	if (XE_IOCTL_ERR(xe, !capable(CAP_SYS_ADMIN)))
		return -EPERM;

	if (XE_IOCTL_ERR(xe, args->extensions))
		return -EINVAL;

	if (XE_IOCTL_ERR(xe, args->flags & ~VALID_MMIO_FLAGS))
		return -EINVAL;

	if (XE_IOCTL_ERR(xe, !(args->flags & DRM_XE_MMIO_WRITE) && args->value))
		return -EINVAL;

	bits_flag = args->flags & DRM_XE_MMIO_BITS_MASK;
	bytes = 1 << bits_flag;
	if (XE_IOCTL_ERR(xe, args->addr + bytes > xe->mmio.size))
		return -EINVAL;

	if (args->flags & DRM_XE_MMIO_WRITE) {
		switch (bits_flag) {
		case DRM_XE_MMIO_8BIT:
			return -EINVAL; /* TODO */
		case DRM_XE_MMIO_16BIT:
			return -EINVAL; /* TODO */
		case DRM_XE_MMIO_32BIT:
			if (XE_IOCTL_ERR(xe, args->value > U32_MAX))
				return -EINVAL;
			xe_mmio_write32(to_gt(xe), args->addr, args->value);
			break;
		case DRM_XE_MMIO_64BIT:
			return -EINVAL; /* TODO */
		default:
			WARN(1, "Invalid MMIO bit size");
			return -EINVAL;
		}
	}

	if (args->flags & DRM_XE_MMIO_READ) {
		switch (bits_flag) {
		case DRM_XE_MMIO_8BIT:
			return -EINVAL; /* TODO */
		case DRM_XE_MMIO_16BIT:
			return -EINVAL; /* TODO */
		case DRM_XE_MMIO_32BIT:
			args->value = xe_mmio_read32(to_gt(xe), args->addr);
			break;
		case DRM_XE_MMIO_64BIT:
			args->value = xe_mmio_read64(to_gt(xe), args->addr);
			break;
		default:
			WARN(1, "Invalid MMIO bit size");
			return -EINVAL;
		}
	}

	return 0;
}
