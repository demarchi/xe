/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright © 2021 Intel Corporation
 */

#include "xe_ggtt.h"

#include <linux/sizes.h>
#include <drm/i915_drm.h>

#include <drm/drm_managed.h>

#include "xe_bo.h"
#include "xe_gt.h"
#include "xe_mmio.h"
#include "xe_wopcm.h"

#include "../i915/i915_reg.h"
#include "../i915/gt/intel_gt_regs.h"

static uint64_t gen8_pte_encode(struct xe_bo *bo, uint64_t bo_offset)
{
	uint64_t pte;
	bool is_lmem;

	pte = xe_bo_addr(bo, bo_offset, GEN8_PAGE_SIZE, &is_lmem);
	pte |= GEN8_PAGE_PRESENT;

	if (is_lmem)
		pte |= GEN12_GGTT_PTE_LM;

	return pte;
}

static unsigned int probe_gsm_size(struct pci_dev *pdev)
{
	uint16_t gmch_ctl, ggms;

	pci_read_config_word(pdev, SNB_GMCH_CTRL, &gmch_ctl);
	ggms = (gmch_ctl >> BDW_GMCH_GGMS_SHIFT) & BDW_GMCH_GGMS_MASK;
	return ggms ? SZ_1M << ggms : 0;
}

static void xe_ggtt_set_pte(struct xe_ggtt *ggtt, uint64_t addr, uint64_t pte)
{
	XE_BUG_ON(addr & GEN8_PTE_MASK);
	XE_BUG_ON(addr >= ggtt->size);

	writeq(pte, &ggtt->gsm[addr >> GEN8_PTE_SHIFT]);
}

static void xe_ggtt_clear(struct xe_ggtt *ggtt, uint64_t start, uint64_t size)
{
	uint64_t end = start + size - 1;
	uint64_t scratch_pte;

	XE_BUG_ON(start >= end);

	scratch_pte = gen8_pte_encode(ggtt->scratch, 0);

	while (start < end) {
		xe_ggtt_set_pte(ggtt, start, scratch_pte);
		start += GEN8_PAGE_SIZE;
	}
}

static void ggtt_fini(struct drm_device *drm, void *arg)
{
	struct xe_ggtt *ggtt = arg;

	mutex_destroy(&ggtt->lock);
	drm_mm_takedown(&ggtt->mm);

	xe_bo_unpin_map_no_vm(ggtt->scratch);

	iounmap(ggtt->gsm);
}

#define XEHPSDV_TILE0_ADDR_RANGE		_MMIO(0x4900)
#define XEHPSDV_TILE1_ADDR_RANGE		_MMIO(0x4904)
#define XEHPSDV_TILE2_ADDR_RANGE		_MMIO(0x4908)
#define XEHPSDV_TILE3_ADDR_RANGE		_MMIO(0x490C)
#define XEHPSDV_TILE_LMEM_RANGE_SHIFT		8
#define XEHPSDV_TILE_LMEM_BASE_SHIFT		1
#define XEHPSDV_TILE_LMEM_BASE_MASK		REG_GENMASK(7, 1)
#define XEHPSDV_TILE_LMEM_RANGE_MASK		REG_GENMASK(14, 8)

int xe_ggtt_init(struct xe_gt *gt, struct xe_ggtt *ggtt)
{
	struct xe_device *xe = gt_to_xe(gt);
	struct pci_dev *pdev = to_pci_dev(xe->drm.dev);
	unsigned int gsm_size;
	phys_addr_t phys_addr;
	int err;

	ggtt->gt = gt;

	gsm_size = probe_gsm_size(pdev);
	if (gsm_size == 0) {
		drm_err(&xe->drm, "Hardware reported no preallocated GSM\n");
		return -ENOMEM;
	}

	if (xe->info.tile_count > 1) {
		static const i915_reg_t tile_addr_reg[] = {
			XEHPSDV_TILE0_ADDR_RANGE,
			XEHPSDV_TILE1_ADDR_RANGE,
			XEHPSDV_TILE2_ADDR_RANGE,
			XEHPSDV_TILE3_ADDR_RANGE,
		};
		u64 lmem_size, lmem_base;
		u64 lmr = xe_mmio_read64(gt, tile_addr_reg[0].reg) & 0xffff;
		u32 stolen = xe_mmio_read64(gt, GEN6_STOLEN_RESERVED.reg);
		u64 dsm_base = xe_mmio_read64(gt, GEN12_DSMBASE.reg);

		drm_info(&xe->drm, "XEHPSDV_TILE0_ADDR_RANGE = %llx\n", lmr);
		drm_info(&xe->drm, "GEN6_STOLEN_RESERVED = %08x\n", stolen);
		drm_info(&xe->drm, "GEN12_DSMBASE = %llx\n", dsm_base);
		lmem_size = lmr >> XEHPSDV_TILE_LMEM_RANGE_SHIFT;
		lmem_base = (lmr & 0xFF) >> XEHPSDV_TILE_LMEM_BASE_SHIFT;
		lmem_size *= SZ_1G;
		lmem_base *= SZ_1G;
		drm_dbg(&xe->drm, "tile%d: 0x%llx 0x%llx\n", 0, lmem_base, lmem_size);
		phys_addr = pci_resource_start(pdev, 0) +
			    pci_resource_len(pdev, 0) / (2 * xe->info.tile_count);
	} else
		/* For Modern GENs the PTEs and register space are split in the BAR */
		phys_addr = pci_resource_start(pdev, 0) + pci_resource_len(pdev, 0) / 2;
	ggtt->gsm = ioremap(phys_addr, gsm_size);
	if (!ggtt->gsm) {
		drm_err(&xe->drm, "Failed to map the ggtt page table\n");
		return -ENOMEM;
	}

	ggtt->scratch = xe_bo_create_locked(xe, NULL, GEN8_PAGE_SIZE,
					    ttm_bo_type_kernel,
					    XE_BO_CREATE_VRAM_IF_DGFX(xe));
	if (IS_ERR(ggtt->scratch)) {
		err = PTR_ERR(ggtt->scratch);
		goto err_iomap;
	}

	err = xe_bo_pin(ggtt->scratch);
	xe_bo_unlock_no_vm(ggtt->scratch);
	if (err)
		goto err_scratch;

	ggtt->size = (gsm_size / 8) * (uint64_t)GEN8_PAGE_SIZE;
	xe_ggtt_clear(ggtt, 0, ggtt->size - 1);

	/*
	 * 8B per entry, each points to a 4KB page.
	 *
	 * The GuC owns the WOPCM space, thus we can't allocate GGTT address in
	 * this area. Even though we likely configure the WOPCM to less than the
	 * maximum value, to simplify the driver load (no need to fetch HuC +
	 * GuC firmwares and determine there sizes before initializing the GGTT)
	 * just start the GGTT allocation above the max WOPCM size. This might
	 * waste space in the GGTT (WOPCM is 2MB on modern platforms) but we can
	 * live with this.
	 *
	 * Another benifit of this is the GuC bootrom can't access anything
	 * below the WOPCM max size so anything the bootom needs to access (e.g.
	 * a RSA key) needs to be placed in the GGTT above the WOPCM max size.
	 * Starting the GGTT allocations above the WOPCM max give us the correct
	 * placement for free.
	 */
	drm_mm_init(&ggtt->mm, xe_wopcm_size(xe),
		    ggtt->size - xe_wopcm_size(xe));
	mutex_init(&ggtt->lock);

	err = drmm_add_action_or_reset(&xe->drm, ggtt_fini, ggtt);
	if (err)
		return err;

	return 0;

err_scratch:
	xe_bo_put(ggtt->scratch);
err_iomap:
	iounmap(ggtt->gsm);
	return err;
}

#define GEN12_GUC_TLB_INV_CR                     _MMIO(0xcee8)
#define   GEN12_GUC_TLB_INV_CR_INVALIDATE        (1 << 0)

static void xe_ggtt_invalidate(struct xe_gt *gt)
{
	/* TODO: vfunc for GuC vs. non-GuC */

	/* TODO: i915 makes comments about this being uncached and
	 * therefore flushing WC buffers.  Is that really true here?
	 */
	xe_mmio_write32(gt, GFX_FLSH_CNTL_GEN6.reg, GFX_FLSH_CNTL_EN);
	if (xe_gt_guc_submission_enabled(gt))
		xe_mmio_write32(gt, GEN12_GUC_TLB_INV_CR.reg,
				GEN12_GUC_TLB_INV_CR_INVALIDATE);
}

void xe_ggtt_printk(struct xe_ggtt *ggtt, const char *prefix)
{
	uint64_t addr, scratch_pte;

	scratch_pte = gen8_pte_encode(ggtt->scratch, 0);

	printk("%sGlobal GTT:", prefix);
	for (addr = 0; addr < ggtt->size; addr += GEN8_PAGE_SIZE) {
		unsigned int i = addr / GEN8_PAGE_SIZE;

		XE_BUG_ON(addr > U32_MAX);
		if (ggtt->gsm[i] == scratch_pte)
			continue;

		printk("%s    ggtt[0x%08x] = 0x%016llx",
		       prefix, (uint32_t)addr, ggtt->gsm[i]);
	}
}

int xe_ggtt_insert_special_node(struct xe_ggtt *ggtt, struct drm_mm_node *node, u32 size, u32 align)
{
	int ret;

	mutex_lock(&ggtt->lock);
	ret = drm_mm_insert_node_generic(&ggtt->mm, node, size, align, 0, DRM_MM_INSERT_HIGH);
	mutex_unlock(&ggtt->lock);

	return ret;
}

int xe_ggtt_insert_bo(struct xe_ggtt *ggtt, struct xe_bo *bo)
{
	uint64_t offset, pte;
	int err;

	if (XE_WARN_ON(bo->ggtt_node.size)) {
		/* Someone's already inserted this BO in the GGTT */
		XE_BUG_ON(bo->ggtt_node.size != bo->size);
		return 0;
	}

	err = xe_bo_populate(bo);
	if (err)
		return err;

	mutex_lock(&ggtt->lock);

	err = drm_mm_insert_node(&ggtt->mm, &bo->ggtt_node, bo->size);
	if (!err) {
		uint64_t start = bo->ggtt_node.start;

		for (offset = 0; offset < bo->size; offset += GEN8_PAGE_SIZE) {
			pte = gen8_pte_encode(bo, offset);
			xe_ggtt_set_pte(ggtt, start + offset, pte);
		}
	}

	xe_ggtt_invalidate(ggtt->gt);

	mutex_unlock(&ggtt->lock);

	return 0;
}

void xe_ggtt_remove_node(struct xe_ggtt *ggtt, struct drm_mm_node *node)
{
	mutex_lock(&ggtt->lock);

	xe_ggtt_clear(ggtt, node->start, node->size);
	drm_mm_remove_node(node);
	node->size = 0;

	xe_ggtt_invalidate(ggtt->gt);

	mutex_unlock(&ggtt->lock);
}

void xe_ggtt_remove_bo(struct xe_ggtt *ggtt, struct xe_bo *bo)
{
	if (XE_WARN_ON(!bo->ggtt_node.size))
		return;

	/* This BO is not currently in the GGTT */
	XE_BUG_ON(bo->ggtt_node.size != bo->size);

	xe_ggtt_remove_node(ggtt, &bo->ggtt_node);
}
